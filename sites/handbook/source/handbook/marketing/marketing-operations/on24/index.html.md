---
layout: handbook-page-toc
title: "ON24"
description: "ON24 is a sales and marketing platform for digital engagement, delivering insights to drive ​revenue growth." 
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}


# About ON24

Page in progress - Recently purchased. Marketing Operations is in the process of integrating and implementing. Follow along in [epic](https://gitlab.com/groups/gitlab-com/-/epics/1800). 

## Provisioning

We have a limited number of seats. Before putting in an Access Request, please [open an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=on24_access_request) for Marketing Operations to review your request.   Once your request is approved, then proceed to open an [Access Request]([create one](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request)

### User Roles
ON24 licenses are only required for people who are building webinar programs (presenters/speakers do not need a license). We have a limited number of licenses and adding more comes with a price.

**Platform Login: Administrative Access** (may also be referred to as a `license` or `seat`)
- The Platform Login has full access to Elite and all functionality, including the ability to create and edit webinars
- These logins are ONLY meant for those creating, setting up, and developing webinars / events
- The ability to edit / change logins is available

**Presenter Roles: URL/Link Access**
- Presenters may log in as Producer, Presenter, or a Q&A Moderator
- All three roles will access the event via a unique link for each event

The chart below outlines the user permissions for each role (Producer, Presenter, and Q&A moderator)

|   | Producer | Presenter | Q&A Moderator |
| - | -------- | --------- | -------------|
| Upload slide and videos | :white_check_mark: |  |  |
| Add poll questions | :white_check_mark: |  |  |
| Start/stop the webinar | :white_check_mark: |  |  |
| Arrange presentation materials | :white_check_mark: |  |  |
| Change webcam layouts | :white_check_mark: |  |  |
| Mute and disconnect presenters | :white_check_mark: |  |  |
| Advance slides | :white_check_mark: | :white_check_mark: |  |
| Push poll questions to the audience | :white_check_mark: | :white_check_mark: |  |
| Whiteboarding tools | :white_check_mark: | :white_check_mark: |  |
| Screenshare | :white_check_mark: | :white_check_mark: |  |
| Q&A | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Team chat | :white_check_mark: | :white_check_mark: | :white_check_mark: |

## Resources 

Please note, you will not be able to access these trainings until you have an ON24 login.  You can request access by opening a [MOps issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=on24_access_request).

- [Getting Started with Webcast Elite Certification](https://training.on24.com/standard-training)
- [Preparing Your Webinar Content](https://training.on24.com/preparing-your-webinar-content)
- [Presenter Certification & Training Program](https://training.on24.com/presenter-certification-new-improved)
- [ON24 Analytics 101](https://training.on24.com/analytics-101) and [Analytics 102](https://training.on24.com/analytics-102)
- [ON24 Knowledge Center](https://on24support.force.com/Support/s/knowledge)
- [ON24 Office Hours](https://training.on24.com/office-hours-webinars?next=%2Foffice-hours-webinars%2F869178) 

## Customizing Your CTA

Each ON24 Console Templates will have a placeholder for a CTA. It is up to the DRI for the webcast to decide what the CTA is. In order to customize the CTA, following these steps:

1. [Log in to ON24](https://wcc.on24.com/webcast/login)
1. If you have not already, create a copy of the console template you wish to use by clicking the `create copy` icon on the right side of the page
1. Once you've created the copy, add the details of your webcast in the `Overview` section. Then select `Console Builder` from the left hand menu
1. Once in the console, click on the `CTA Placeholder` icon from the bottom menu. This icon looks like a pointer finger with signal bars above it. You will see `CTA Placeholder` when you hover over it. 
1. When the `CTA Placeholder` wdget opens in the console, click on the gear in the right hand corner of the widget window. 
1. Click on `Attributes`. Update the `Name` field and select/deselect any of the options as needed. 
1. Click the gear again and select `Configuration`. Update the `Text`, `Button Text`, and `URL`. 

All changes are auto-saved. 
