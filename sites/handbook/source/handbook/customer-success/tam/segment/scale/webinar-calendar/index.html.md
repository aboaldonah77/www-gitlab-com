---
layout: handbook-page-toc
title: "TAM Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

## Upcoming Webinars

We’d like to invite you to our free upcoming webinars during the month of August and September.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

### August 2022

### Introducción a CI/CD con GitLab
#### August 18th, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

Ven y conoce qué es CI/CD y cómo puede beneficiar a tu equipo/organización. Cubriremos una descripción general de CI/CD y cómo se ve en GitLab. También cubriremos cómo comenzar con su primer pipeline de CI/CD en GitLab y los conceptos básicos de los GitLab Runners.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yfkTIB-pSl2EFkkk4NMc8w)

### Advanced CI/CD
#### August 23rd, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_pUbCQeh9T8GAATWdKI7feQ)

### GitLab CI/CD Avanzado
#### August 23rd, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

Amplíe su conocimiento de CI/CD mientras cubrimos temas avanzados que acelerarán su eficiencia usando GitLab, como pipelines, variables, reglas, artefactos y más. Esta sesión está destinada a aquellos que han usado CI/CD en el pasado.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-R0SBQYwR1G408B7ZSIjQw)

### DevSecOps con GitLab
#### August 25th, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

GitLab permite que los desarrolladores y la seguridad trabajen juntos en una sola herramienta, lo que permite una seguridad proactiva o "desplazamiento a la izquierda". Esta sesión cubrirá lo que ofrece GitLab, cómo los resultados del escaneo se integran sin problemas con los Merge Requests y cómo usar el panel de seguridad para administrar las vulnerabilidades detectadas.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_dgCOdHnaQrCers4lWd-a1Q)

### September 2022

### Intro to GitLab
#### September 7th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yDIlG884SISbzaTyolRWSA)

### Intro to CI/CD 
#### September 13th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_RUr9MxU6SEa7H2rhZ-2YEQ)

### Advanced CI/CD
#### September 20th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BXFJdbsNRRq2ii-cW4mAOg)

### DevSecOps/Compliance
#### September 27th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_vowi9PkyRQWpJUHuU93CJw)

### Getting Started with Continuous Delivery and GitOps
#### September 29th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

In this webinar you will learn the concepts behind continuous delivery, continuous deployment and GitOps. You will also learn how to set up a DevOps pipeline that deploys your application to several environments on Kubernetes.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_S7hSXhryT4eTwWxH1lCFgQ)

